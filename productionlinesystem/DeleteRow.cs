﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace APP
{
    public partial class DeleteRow : RowOperation
    {
        public DeleteRow()
        {
            InitializeComponent();
        }
        public void setdata(DataGridView obj)
        {
            foreach (DataGridViewRow row in obj.SelectedRows)
            {
                this.dataGridView.Rows.Add(new object[] { row.Cells[0].Value, row.Cells[1].Value, row.Cells[2].Value, row.Cells[3].Value });
            }
        }
        protected override void button1_Click(object sender, EventArgs e)
        {
            //for (int i = 0; i < dataGridView.RowCount-1; i++)
            //{
            //    DataGridViewRow obj = dataGridView.Rows[i];
            //    userdata.DelUserData(obj);
            //}
            userdata.DelUserData(dataGridView);

            this.Close();
        }
    }
}
