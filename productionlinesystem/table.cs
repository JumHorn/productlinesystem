﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace APP
{
    public partial class table : Form
    {
        public table()
        {
            InitializeComponent();
        }

        private void findbutton_Click(object sender, EventArgs e)
        {
            //清除原有数据
            UserdataGridView.Rows.Clear();

            foreach (var user in userdata.GetUserData())
            {
                UserdataGridView.Rows.Add(user);
                //this.dataGridView1.Rows[index].Cells[0].Value = user.id;
                //this.dataGridView1.Rows[index].Cells[1].Value = user.username;
                //this.dataGridView1.Rows[index].Cells[2].Value = user.password;
                //this.dataGridView1.Rows[index].Cells[3].Value = user.gender;
            }
        }

        private void Addbutton_Click(object sender, EventArgs e)
        {
            DataGridViewRow dr = new DataGridViewRow();
            UserdataGridView.Rows.Insert(0, dr);
            foreach (DataGridViewRow row in UserdataGridView.SelectedRows)
            {
                row.Selected = false;
            }
            UserdataGridView.Rows[0].Selected = true;
            RowOperation addrow = new RowOperation();
            //不在任务栏上显示
            addrow.ShowInTaskbar = false;
            //以对话框形式显示
            addrow.ShowDialog();
        }

        private void UserdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
