﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace APP
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void findbutton_Click(object sender, EventArgs e)
        {
            //UserdataGridView.Rows.Clear();

            //foreach (var user in userdata.GetUserData())
            //{
            //    UserdataGridView.Rows.Add(user);
            //}
            userdata.refresh(UserdataGridView);
        }

        private void Addbutton_Click(object sender, EventArgs e)
        {
            //DataGridViewRow dr = new DataGridViewRow();
            //UserdataGridView.Rows.Insert(0, dr);
            //foreach (DataGridViewRow row in UserdataGridView.SelectedRows)
            //{
            //    row.Selected = false;
            //}
            //UserdataGridView.Rows[0].Selected = true;
            AddRow addrow = new AddRow();
            //不在任务栏上显示
            //addrow.ShowInTaskbar = false;
            //以对话框形式显示
            addrow.ShowDialog();
            userdata.refresh(UserdataGridView);
        }

        private void Modifybutton_Click(object sender, EventArgs e)
        {
            ModifyRow chrow = new ModifyRow();
            chrow.setdata(UserdataGridView);
            chrow.ShowDialog();
            userdata.refresh(UserdataGridView);
        }

        private void Deletebutton_Click(object sender, EventArgs e)
        {
            DeleteRow delrow = new DeleteRow();
            //foreach (DataGridViewRow row in UserdataGridView.SelectedRows)
            //{
            //    delrow.setdata(row);
            //}
            delrow.setdata(UserdataGridView);
            delrow.ShowDialog();

            userdata.refresh(UserdataGridView);
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //SaveFileDialog sfd = new SaveFileDialog();
            userdata.ExportExcel("data",UserdataGridView);
            //sfd.ShowDialog();
        }

        private void 打开ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }
    }
}
