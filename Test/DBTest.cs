﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils;
using Model;
using DAL;

namespace Test
{
    [TestClass]
    public class DBTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Console.WriteLine(ConfigHelper.AppConfig.DbConnStr);
            DBHelper db = new DBHelper(ConfigHelper.AppConfig.DbConnStr);
            if (!db.IsUseful())
            {
                Console.WriteLine("数据库连接失败");
            }
            else
            {
                Console.WriteLine("数据库连接成功");
            }
        }
    }
}
