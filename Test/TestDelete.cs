﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using Model;
using Utils;

namespace Test
{
    [TestClass]
    public class TestDelete
    {
        [TestMethod]
        public void TestMethod()
        {
            var testuser = new TestUser
            {
                id=11,
                username="Jum"
            };
            QueryHelper sql = new QueryHelper();
            //sql.JsonQuery();
            sql.ObjectQuery<TestUser>(testuser);
            sql.FormatAnd();

            ModelDBHelper<TestUser>.Delete(sql);

            //var checkuser = new TestUser
            //{
            //    username = "asdsad"
            //};
            //QueryHelper check = new QueryHelper();
            //check.ObjectQuery<TestUser>(checkuser);
            //check.FormatWhere();
            var queryuser = ModelDBHelper<TestUser>.Find();

            foreach (var n in queryuser)
            {
                Assert.AreNotEqual<string>(n.password, "Jum");
            }
        }
    }
}
