﻿using System;
using DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Utils;

namespace Test
{
    //测试查找方法
    [TestClass]
    public class TestFind
    {
        [TestMethod]
        public void TestMethod()
        {
            foreach (var user in ModelDBHelper<TestUser>.Find())
            {
                Console.WriteLine(user);
            }
            Console.WriteLine(ModelDBHelper<TestUser>.Find());
        }
    }
}