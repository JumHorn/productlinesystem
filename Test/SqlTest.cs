﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using Utils;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Test
{
    [TestClass]
    public class SqlTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            DBHelper dbhelper = new DBHelper(ConfigHelper.AppConfig.DbConnStr);
            string readsql = string.Format("update [{0}] set {1} where {2}", GetTableName(), @"username=@username",@"gender=@gender");
            Console.WriteLine(readsql);
            var result = dbhelper.ExecuteSql(readsql, GetAndSql().ToArray());  //返回受影响的行数
        }

        string GetTableName()
        {
            return "user";
        }
        List<SqlParameter> GetAndSql()
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("username", "Jum"));
            parameter.Add(new SqlParameter("gender", "f"));
            return parameter;
        }
        string GetOrSql()
        {
            return "";
        }
    }
}
