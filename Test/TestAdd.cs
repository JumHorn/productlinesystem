﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Utils;
using DAL;

namespace Test
{
    [TestClass]
    public class TestAdd
    {
        [TestMethod]
        public void TestMethod1()
        {
            var testuser = new TestUser
            {
                id = 2,
                username = "asdsad",
                password = "asdsadasdas",
                gender = "f"
            };
            QueryHelper sql = new QueryHelper();
            sql.ObjectQuery<TestUser>(testuser);
            sql.FormatToAdd();
            //测试add方法
            ModelDBHelper<TestUser>.Add(sql);

            var checkuser = new TestUser
            {
                username = "asdsad"
            };
            QueryHelper check = new QueryHelper();
            check.ObjectQuery<TestUser>(checkuser);
            check.FormatAnd();
            var queryuser = ModelDBHelper<TestUser>.Find(check);

            foreach (var n in queryuser)
            {
                Assert.Equals(testuser.password, n.password);
                Assert.Equals(testuser.gender, n.gender);
            }
        }
    }
}
