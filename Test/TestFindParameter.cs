﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils;
using Model;
using DAL;

namespace Test
{
    [TestClass]
    public class TestFindParameter
    {
        [TestMethod]
        public void TestMethod1()
        {
            QueryHelper sql = new QueryHelper();
            sql.Jsonquery = @"{""gender"": ""m"",""username"":""Jum""}";
            sql.JsonQuery();
            sql.FormatAnd();

            foreach (var user in ModelDBHelper<TestUser>.Find(sql))
            {
                Console.WriteLine(user);
            }
            Console.WriteLine(ModelDBHelper<TestUser>.Find(sql));
        }
    }
}
