﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Model;
using System.Windows.Forms;
using Utils;

namespace BLL
{
    public class userdata
    {
        public static void refresh(DataGridView obj)
        {
            obj.Rows.Clear();
            foreach (var user in userdata.GetUserData())
            {
                obj.Rows.Add(user);
            }
        }
        public static IList<object[]> GetUserData()
        {
            List<object[]> obj = new List<object[]>();
            foreach (var user in ModelDBHelper<TestUser>.Find())
            {
                obj.Add(new object[]{ user.id, user.username, user.password, user.gender });
            }
            return obj;
        }

        public static void DelUserData(DataGridView obj)
        {
            for(int i = 0; i < obj.RowCount-1; i++)
            {
                //delete这里可以性能优化，以id作为主键不可更改，检索id即可
                var testuser = new TestUser
                {
                    id = (int)obj.Rows[i].Cells[0].Value,
                    username = (string)obj.Rows[i].Cells[1].Value,
                    password = (string)obj.Rows[i].Cells[2].Value,
                    gender = (string)obj.Rows[i].Cells[3].Value
                };

                QueryHelper sql = new QueryHelper();
                sql.ObjectQuery<TestUser>(testuser);
                sql.FormatAnd();

                ModelDBHelper<TestUser>.Delete(sql);
            }

        }
        public static void UpdateUserData(DataGridView obj)
        {
            for (int i = 0; i < obj.RowCount - 1; i++)
            {
                var testuser = new TestUser
                {
                    //id = (int)obj.Rows[i].Cells[0].Value,
                    username = (string)obj.Rows[i].Cells[1].Value,
                    password = (string)obj.Rows[i].Cells[2].Value,
                    gender = (string)obj.Rows[i].Cells[3].Value
                };

                QueryHelper sql = new QueryHelper();
                sql.ObjectQuery<TestUser>(testuser);
                sql.FormatToUpdate();

                ModelDBHelper<TestUser>.Upadate(sql, string.Format("id={0}", (int)obj.Rows[i].Cells[0].Value));
            }
        }
        public static void AddUserData(DataGridView obj)
        {
            for (int i = 0; i < obj.RowCount - 1; i++)
            {
                var testuser = new TestUser
                {
                    id = Convert.ToInt32(obj.Rows[i].Cells[0].Value),
                    username = (string)obj.Rows[i].Cells[1].Value,
                    password = (string)obj.Rows[i].Cells[2].Value,
                    gender = (string)obj.Rows[i].Cells[3].Value
                };

                QueryHelper sql = new QueryHelper();
                sql.ObjectQuery<TestUser>(testuser);
                sql.FormatToAdd();

                ModelDBHelper<TestUser>.Add(sql);
            }
        }
        /// <summary>
        /// datagridview 转excel
        /// </summary>
        /// <param name="datagridview"></param>
        public static void ExportExcel(string fileName, DataGridView datagridview)
        {
            //打开文件对话框
            string saveFileName = "";
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.DefaultExt = "xlsx";
            saveDialog.Filter = "Excel文件|*.xlsx";
            saveDialog.FileName = fileName;
            saveDialog.ShowDialog();
            saveFileName = saveDialog.FileName;

            if (saveFileName.IndexOf(":") < 0) return; //被点了取消
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            if (xlApp == null)
            {
                MessageBox.Show("无法创建Excel对象，可能您的机子未安装Excel");
                return;
            }
            Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
            Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];//取得sheet1
            //写入标题
            for (int i = 0; i < datagridview.ColumnCount; i++)
            {
                worksheet.Cells[1, i + 1] = datagridview.Columns[i].HeaderText;
            }
            //写入数值
            for (int r = 0; r < datagridview.Rows.Count; r++)
            {
                for (int i = 0; i < datagridview.ColumnCount; i++)
                {
                    worksheet.Cells[r + 2, i + 1] = datagridview.Rows[r].Cells[i].Value;
                }
                System.Windows.Forms.Application.DoEvents();
            }
            worksheet.Columns.EntireColumn.AutoFit();//列宽自适应
            if (saveFileName != "")
            {
                try
                {
                    workbook.Saved = true;
                    workbook.SaveCopyAs(saveFileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("导出文件时出错,文件可能正被打开！\n" + ex.Message);
                }
            }
            xlApp.Quit();
            GC.Collect();//强行销毁
            //MessageBox.Show("文件： " + fileName + ".xls 保存成功", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
