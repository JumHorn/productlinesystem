﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Model;
using Utils;
using System.Windows.Forms;

namespace DAL
{
    public class ModelDBHelper<T>
    {
        private static DBHelper dbhelper;

        /// <summary>
        /// 创建db的连接字符串
        /// </summary>
        private static void GetDbHelper()
        {
            bool local = IsLocalDB();
            string dbname = GetDbName();
            string connstr = string.Format("Data Source=.;DataBase={0};integrated security=SSPI",dbname);
            //string connstr = $"Data Source=.;DataBase={dbname};integrated security=SSPI;";

            if (!local)
                connstr = ConfigHelper.AppConfig.DbConnStr; 
            dbhelper = new DBHelper(connstr);
        }

        /// <summary>
        /// 用反射方法从特性中获取dbname
        /// </summary>
        /// <returns></returns>
        private static string GetDbName()
        {
            var database = typeof(T).GetCustomAttributes(typeof(DBAttribute), false).First() as DBAttribute;
            string dbname = database.dataBase;
            if (string.IsNullOrEmpty(dbname))
                //throw new Exception($"Type:{typeof(T).FullName},取数据库字段失败或无DBAttribute属性！");
                throw new Exception(string.Format("Type:{0},取数据库字段失败或无DBAtribute属性！",typeof(T).FullName));
            return dbname;
        }

        /// <summary>
        /// 用反射方法从特性中获取是否本地
        /// </summary>
        /// <returns></returns>
        private static bool IsLocalDB()
        {
            var database = typeof(T).GetCustomAttributes(typeof(DBAttribute), false).First() as DBAttribute;
            return database.local;
        }

        /// <summary>
        /// 用反射方法获取表名
        /// </summary>
        /// <returns></returns>
        public static string GetTableName()
        {
            var table = typeof(T).GetCustomAttributes(typeof(TableAttribute), false).First() as TableAttribute;
            string tablename = table.tableName;
            if (string.IsNullOrEmpty(tablename))
                //throw new Exception($"Type:{typeof(T).FullName},取表格字段失败或无TableAttribute属性！");
                throw new Exception(string.Format("Type:{0},取表格字段失败或无TableAtribute属性！",typeof(T).FullName));
            return tablename;
        }

        //封装简单地增删改查

        public static IList<T> Find()   //封装TSqlQuery类，实现反序列化
        {
            if (null == dbhelper)
                GetDbHelper();

            var type = typeof(T);
            var props = type.GetProperties();

            if (!dbhelper.IsUseful())
            {
                //Console.WriteLine($"Type:{type.FullName},数据连接不可用！");
                Console.WriteLine(string.Format("Type:{0},数据连接不可用！", type.FullName));
                return null;
            }


            //string readsql = $"select * from [{GetTableName()}]";
            string readsql = string.Format("select * from [{0}]", GetTableName());
            var table = dbhelper.GetDataSet(readsql).Tables[0];  //结果集的第一张表

            return table.AsEnumerable().Select(row =>
                {
                    var instance = Activator.CreateInstance<T>();
                    foreach (var p in props)
                    {
                        string columnName = p.Name;
                        var columnAttrs = type.GetCustomAttributes(typeof(ColumnAttribute), false);
                        if (columnAttrs.Length > 0)
                            columnName = (columnAttrs[0] as ColumnAttribute).Name;
                        if (row[columnName] is DBNull)
                            continue;
                        p.SetValue(instance, row[columnName], null);
                    }
                    return instance;
                }
            ).ToList();
        }

        /// <summary>
        /// sql语句参数化查询（防止sql注入）
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static IList<T> Find(QueryHelper sql)  
        {
            if (null == dbhelper)
                GetDbHelper();

            var type = typeof(T);
            var props = type.GetProperties();

            if (!dbhelper.IsUseful())
            {
                //Console.WriteLine($"Type:{type.FullName},数据连接不可用！");
                Console.WriteLine(string.Format("Type:{0},数据连接不可用！", type.FullName));
                return null;
            }

            //string readsql = $"select * from [{GetTableName()}]";
            string readsql = string.Format("select * from [{0}] where {1}", GetTableName(),sql.sqlstring);
            var table = dbhelper.GetDataSet(readsql,sql.parameter.ToArray()).Tables[0];  //结果集的第一张表

            return table.AsEnumerable().Select(row =>
            {
                var instance = Activator.CreateInstance<T>();
                foreach (var p in props)
                {
                    string columnName = p.Name;
                    var columnAttrs = type.GetCustomAttributes(typeof(ColumnAttribute), false);
                    if (columnAttrs.Length > 0)
                        columnName = (columnAttrs[0] as ColumnAttribute).Name;
                    p.SetValue(instance, row[columnName], null);
                }
                return instance;
            }
            ).ToList();
        }

        public static void Delete(QueryHelper sql)
        {
            if (null == dbhelper)
                GetDbHelper();

            if (!dbhelper.IsUseful())
            {
                //Console.WriteLine($"Type:{type.FullName},数据连接不可用！");
                Console.WriteLine(string.Format("Type:{0},数据连接不可用！", typeof(T).FullName));
                return;
            }

            //string readsql = $"select * from [{GetTableName()}]";
            string readsql = string.Format("delete from [{0}] where {1}", GetTableName(), sql.sqlstring);
            var result = dbhelper.ExecuteSql(readsql, sql.parameter.ToArray());  //返回受影响的行数
        }

        public static void Add(QueryHelper sql)
        {
            if (null == dbhelper)
                GetDbHelper();

            if (!dbhelper.IsUseful())
            {
                //Console.WriteLine($"Type:{type.FullName},数据连接不可用！");
                Console.WriteLine(string.Format("Type:{0},数据连接不可用！", typeof(T).FullName));
                return;
            }

            string readsql = string.Format("insert into [{0}] values({1})", GetTableName(), sql.sqlstring);
            var result = dbhelper.ExecuteSql(readsql, sql.parameter.ToArray());  //返回收影响的行数
            if (result == -1)
            {
                MessageBox.Show("insert failed");  //无法插入数据库，可能由于某些字段为空         
            }
        }
        public static void Upadate(QueryHelper sql,string condition)
        {
            if (null == dbhelper)
                GetDbHelper();

            if (!dbhelper.IsUseful())
            {
                //Console.WriteLine($"Type:{type.FullName},数据连接不可用！");
                Console.WriteLine(string.Format("Type:{0},数据连接不可用！", typeof(T).FullName));
                return;
            }

            try
            {
                string readsql = string.Format("update [{0}] set {1} where {2}", GetTableName(), sql.sqlstring, condition);
                var result = dbhelper.ExecuteSql(readsql, sql.parameter.ToArray());  //返回受影响的行数
            }
            catch
            {
                MessageBox.Show("update failed");  //无法插入数据库，可能由于字段非法
            } 
        }
    }
}