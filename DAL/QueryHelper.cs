﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Script.Serialization;


namespace DAL
{
    public class QueryHelper
    {
        private string jsonquery;
        public string Jsonquery
        {
            get
            {
                return jsonquery;
            }
            set
            {
                jsonquery = value;
            }
        }
        private Dictionary<string,object> paramlist;

        public string sqlstring;  //查询语句
        public List<SqlParameter> parameter;  //参数列表

        //从json中获取查询语句
        public void JsonQuery()
        {
            //string configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "condition.json"); //读取json配置到AppConfig
            //if (!File.Exists(configFile)) return;

            if(Jsonquery==null)
                return;
            string jsonstr = Jsonquery;

            paramlist = new Dictionary<string, object>();
            paramlist = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonstr);
        }

        //从对象中获取查询语句

        public void ObjectQuery<T>(T obj)
        {
            paramlist = new Dictionary<string, object>();
            foreach (var prop in typeof(T).GetProperties())
            {
                if (prop.GetValue(obj, null) != null)
                    paramlist[prop.Name] = prop.GetValue(obj, null);
            }
            //如果参数列表为空，需要在where语句后形成true的表达式
            if (paramlist.Count == 0)
            {
                paramlist["0"] = "0";
            }
        }

        //格式化参数,传递给add
        public void FormatToAdd()
        {
            parameter = new List<SqlParameter>();
            StringBuilder sqlstringbuilder = new StringBuilder();
            foreach (var item in paramlist.Keys)
            {
                if (sqlstringbuilder.Length == 0)
                {
                    sqlstringbuilder.Append("@" + item);
                }
                else
                {
                    sqlstringbuilder.Append(" , "+"@" + item);
                }
                parameter.Add(new SqlParameter(item, paramlist[item]));
            }
            sqlstring = sqlstringbuilder.ToString();
        }
        //格式化参数，传递给update
        public void FormatToUpdate()
        {
            parameter = new List<SqlParameter>();
            StringBuilder sqlstringbuilder = new StringBuilder();
            foreach (var item in paramlist.Keys)
            {
                if (paramlist[item] != null)
                {
                    try
                    {
                        if ((int)paramlist[item] == 0)   //这里可以用或，添加默认类型
                        {
                            continue;
                        }
                    }
                    catch
                    {
                        sqlstringbuilder.Append(item + "=" + "@" + item+", ");
                        parameter.Add(new SqlParameter(item, paramlist[item]));
                    }
                }
            }
            sqlstring = sqlstringbuilder.ToString().TrimEnd(' ').TrimEnd(',');
        }
        //格式化参数，传递给其他的查询方法
        public void FormatAnd()
        {
            parameter = new List<SqlParameter>();
            StringBuilder sqlstringbuilder = new StringBuilder();
            foreach (var item in paramlist.Keys)
            {
                //这样每次判断性能低下，可以用trim方法
                if (sqlstringbuilder.Length == 0)
                {
                    sqlstringbuilder.Append(item + "=" + "@" + item);
                }
                else
                {
                    sqlstringbuilder.Append(" and " + item + "=" + "@" + item);
                }
                parameter.Add(new SqlParameter(item, paramlist[item]));
            }
            sqlstring = sqlstringbuilder.ToString();
        }
    }
}
