﻿using System.Collections.Generic;

namespace Model
{
    public class AppConfig
    {
        public string DbKey { get; set; }
        public string DbConnStr { get; set; }
    }
}