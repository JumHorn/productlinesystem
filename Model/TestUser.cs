﻿using System;

namespace Model
{
    [DB("User", false)]
    [Table("User")]
    public class TestUser
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public string gender { get; set; }


        public override string ToString()
        {
            return string.Format("id:{0}, username:{1}, password: {2}, gender: {3}", id, username, password, gender);
        }        
    }
}