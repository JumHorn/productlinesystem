﻿using System;

namespace Model
{
    /// <summary>
    /// 这个特性加在属性前
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        public ColumnAttribute(string name, bool ispk = false)
        {
            Name = name;
            IsPK = ispk;
        }

        public string Name { get; set; }
        public bool IsPK { get; set; }
    }
}