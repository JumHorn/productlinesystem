﻿using System;

namespace Model
{
    /// <summary>
    ///     本地数据库属性
    /// </summary>
    /// AttributeTargets.Class该属性指向类，不能用于方法前
    [AttributeUsage(AttributeTargets.Class)]
    public class DBAttribute : Attribute
    {
        public DBAttribute(string _dataBase, bool _local = true)
        {
            dataBase = _dataBase;
            local = _local;
        }

        public string dataBase { get; set; }
        public bool local { get; set; }
    }
}