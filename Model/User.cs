﻿using System;
namespace Model
{
    [Table("user")]
    public class User
    {
        [Column("id", true)]
        public string Id { get; set; }

        [Column("username")]
        public string Username { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("gender")]
        public int Gender { get; set; }
    }
}