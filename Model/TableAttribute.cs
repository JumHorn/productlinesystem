﻿using System;

namespace Model
{
    /// <summary>
    /// 这个特性加在类前
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        public TableAttribute(string tablename)
        {
            tableName = tablename;
        }

        public string tableName { get; set; }

    }
}