﻿using System;
using System.IO;
using Model;
using Newtonsoft.Json;

namespace Utils
{
    public class ConfigHelper
    {
        private static AppConfig _AppConfig;

        public static AppConfig AppConfig
        {
            get
            {
                if (null == _AppConfig)
                    LoadConfig();
                return _AppConfig;
            }
            private set
            {
                if (value == null) throw new ArgumentNullException("");   //value是外部的值
                _AppConfig = value;
            }
        }


        public static void SetConfig(AppConfig _appConfig)
        {
            _AppConfig = _appConfig;
        }


        public static void LoadConfig()
        {
            string configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config.json"); //读取json配置到AppConfig

            //这里需要提示文件不存在
            if (!File.Exists(configFile)) return;

            string jsonstr = File.ReadAllText(configFile);
            AppConfig = JsonConvert.DeserializeObject<AppConfig>(jsonstr);
        }
    }
}